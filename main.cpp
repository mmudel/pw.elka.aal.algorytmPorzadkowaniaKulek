/*
Plik main.cpp
Zadanie: Szeregowanie kulek na pochylni
Autor: Micha� Mudel
Prowadzacy: mgr in�. Pawe� Radziszewski
*/


#include <iostream>
#include <conio.h>
#include <cstdlib>
#include <ctime>
#include "Pochylnia.h"
#include <fstream>
#include <string>

void algorytmDobregoPrzestawienia(Pochylnia*);
void algorytmGrupowania(Pochylnia*);
void algorytmNaiwny(Pochylnia*);
void generatorKulek(int, char*);
void testAutomatyczny(void(*funkcja) (Pochylnia*), int, int, int);
void testUzytkownika();

using namespace std;

ofstream wyniki;

int main()
{
	srand(time(NULL));
	wyniki.open("wyniki.txt", ofstream::out);
	/*cout << "Dla algorytmu naiwnego" << endl;
	testAutomatyczny(&algorytmNaiwny, 0, 2000, 10);*/
	cout << "Dla algorytmu dobrego przestawienia" << endl;
	testAutomatyczny(&algorytmDobregoPrzestawienia, 1, 5000, 1);
	/*cout << "Dla algorytmu grupowania" << endl;
	testAutomatyczny(&algorytmGrupowania, 2, 30000, 10);
	testUzytkownika();*/
	wyniki.close();
	_getch();
	return 0;
}

void generatorKulek(int n, char* kulki)
{
	int znak;
	for (int i = 0; i < n; i++)
	{
		kulki[i] = 0;
	}
	for (int i = 0; i < 3; i++)
	{
		kulki[(rand()) % n] = 'n';
	}
	for (int i = 0; i < n; i++)
	{
		if (kulki[i] == 'n')
			continue;
		znak = rand() % 3;
		if (znak == 0)
			kulki[i] = 'c';
		else if (znak == 1)
			kulki[i] = 'z';
		else
			kulki[i] = 'n';
	}
	return;
}

void algorytmDobregoPrzestawienia(Pochylnia* pochylnia)	// Algorytm o zlozonosci n^3
{
	int iloscKulek = pochylnia->iloscKulek;
	bool czyBylMax = false;
	int stopienUporzadkowania = 0;
	int ilePrzesuniec = 0;
	int jakaOdleglosc = 0;
	char porzadkowanaKulka = 'c';

	for (int i = 0; i < iloscKulek; ++i)		// szukamy porzadkowanych kulek, nastepnie przestawiamy je na koniec w odpowiedni sposob, a nastepnie przesuwamy ja na sam poczatek
	{
		if (pochylnia->pochylnia[i] == porzadkowanaKulka)		// znalezlismy porzadkowana kulke
		{
			czyBylMax = false;
			if (i != stopienUporzadkowania)						// sprawdzamy czy nie jest juz na odpowiednim miejscu
			{
				jakaOdleglosc = (iloscKulek - stopienUporzadkowania) % 3;				// wyliczamy odleglosc na podstawie ktorej wyznaczymy sposob przesuniecia
				ilePrzesuniec = (iloscKulek - stopienUporzadkowania - 1) / 3;
				if (jakaOdleglosc == 0)
				{
					pochylnia->przestawKulki(i);
					for (int j = 0; j < ilePrzesuniec; ++j)					// przesuwamy kulke z konca na sam poczatek
					{
						pochylnia->przestawKulki(stopienUporzadkowania);
					}
					++stopienUporzadkowania;
				}
				else if (jakaOdleglosc == 1)
				{
					if (i - 2 >= 0 && i - 2 >= stopienUporzadkowania)
					{
						pochylnia->przestawKulki(i - 2);
						for (int j = 0; j < ilePrzesuniec; ++j)
						{
							pochylnia->przestawKulki(stopienUporzadkowania);
						}
						++stopienUporzadkowania;
					}
					else
						pochylnia->przestawKulki(i - 1);
				}
				else if (jakaOdleglosc == 2)
				{
					pochylnia->przestawKulki(i - 1);
					for (int j = 0; j < ilePrzesuniec; ++j)
					{
						pochylnia->przestawKulki(stopienUporzadkowania);
					}
					++stopienUporzadkowania;
				}
			}
			else
				++stopienUporzadkowania;
			i = stopienUporzadkowania - 1;
		}
		else if (i == iloscKulek - 3)		// kolejnych kulek i tak nie bedziemy mogli dobrze przeniesc
		{
			if (czyBylMax == false)					// ale trzeba sprawdzic czy w tych 3 ostatnich nie bylo czerwonej
			{
				czyBylMax = true;
				i = stopienUporzadkowania - 1;
				pochylnia->przestawKulki(stopienUporzadkowania);
			}
			else									// raz juz przetasowalismy i juz nie bylo czerwonej
			{
				if (porzadkowanaKulka == 'c')
				{
					porzadkowanaKulka = 'z';
					i = stopienUporzadkowania - 1;
					czyBylMax = false;
				}
				else
					return;
			}
		}
	}
}

void algorytmGrupowania(Pochylnia* pochylnia)
{
	int iloscKulek = pochylnia->iloscKulek;
	char porzadkowanaKulka = 'c';
	int odlegloscPierwszej = 0;
	int stopienUporzadkowania = 0;
	int ilePrzesuniec = 0;
	int jakaOdleglosc = 0;
	bool czyBylMax = false;
	while (true)
	{
		for (int i = stopienUporzadkowania; ; i++)							//chcemy aby w ostatnich trzech kulkach byla conajmniej jedna porzadkowana
		{
			if (pochylnia->pochylnia[i] == porzadkowanaKulka)
			{
				pochylnia->przestawKulki(i);
				break;
			}
		}
		for (int i = iloscKulek - 1; i > 0; --i)							// szukamy ostatniej od konca kulki
		{
			if (pochylnia->pochylnia[i] == porzadkowanaKulka)
			{
				odlegloscPierwszej = i;
				break;
			}
		}
		if ((odlegloscPierwszej - stopienUporzadkowania) % 3 != 0)							// jesli pierwsza od konca kulka porzadkowanego koloru nie trafi ostatecznie na pierwsze miejsce to ustawiamy ja tak zeby trafila
		{
			if (odlegloscPierwszej + 3 > iloscKulek)					// znaleziona kulka jest jedna z 2 ostatnich
			{
				pochylnia->przestawKulki(stopienUporzadkowania);
				odlegloscPierwszej -= 3;
			}
			if ((iloscKulek - stopienUporzadkowania) % 3 == 0)
			{
				pochylnia->przestawKulki(odlegloscPierwszej);
				odlegloscPierwszej = iloscKulek - 3;
			}
			else if ((iloscKulek - stopienUporzadkowania) % 3 == 1)
			{
				pochylnia->przestawKulki(odlegloscPierwszej - 2);
				odlegloscPierwszej = iloscKulek - 1;
			}
			else
			{
				pochylnia->przestawKulki(odlegloscPierwszej - 1);
				odlegloscPierwszej = iloscKulek - 2;
			}
		}															// kulka juz jest na dobrym miejscu, 'odlegloscPierwszej' -- jej indeks
		ilePrzesuniec = (odlegloscPierwszej - stopienUporzadkowania) / 3;
		for (bool znalezionoCiag = false; ilePrzesuniec > 0; --ilePrzesuniec, znalezionoCiag = false, odlegloscPierwszej -= 3)					// szukamy odpowiednich ciagow i przestawiamy kulki dopoki 'Pierwsza' nie bedzie na poczatku
		{
			if (pochylnia->pochylnia[iloscKulek - 1] == porzadkowanaKulka)
			{
				// 3 zlych, lub dowolnej ilosci wpierw porzadkowanych a potem zlych
				for (int i = stopienUporzadkowania; i < odlegloscPierwszej - 2; i++)
				{
					if ((pochylnia->pochylnia[i] == porzadkowanaKulka && pochylnia->pochylnia[i + 1] == porzadkowanaKulka && pochylnia->pochylnia[i + 2] == porzadkowanaKulka)
						|| (pochylnia->pochylnia[i] == porzadkowanaKulka && pochylnia->pochylnia[i + 1] == porzadkowanaKulka && pochylnia->pochylnia[i + 2] != porzadkowanaKulka)
						|| (pochylnia->pochylnia[i] == porzadkowanaKulka && pochylnia->pochylnia[i + 1] != porzadkowanaKulka && pochylnia->pochylnia[i + 2] != porzadkowanaKulka)
						|| (pochylnia->pochylnia[i] != porzadkowanaKulka && pochylnia->pochylnia[i + 1] != porzadkowanaKulka && pochylnia->pochylnia[i + 2] != porzadkowanaKulka))
					{
						znalezionoCiag = true;
						pochylnia->przestawKulki(i);
						break;
					}
				}
			}
			else if (pochylnia->pochylnia[iloscKulek - 2] == porzadkowanaKulka)
			{
				// szukamy 2 zlych a potem dobrej
				for (int i = stopienUporzadkowania; i < odlegloscPierwszej - 2; i++)
				{
					if (pochylnia->pochylnia[i] != porzadkowanaKulka && pochylnia->pochylnia[i + 1] != porzadkowanaKulka && pochylnia->pochylnia[i + 2] == porzadkowanaKulka)
					{
						znalezionoCiag = true;
						pochylnia->przestawKulki(i);
						break;
					}
				}
			}
			else if (pochylnia->pochylnia[iloscKulek - 3] == porzadkowanaKulka)
			{
				// szukamy 1 zlej, potem dobra a potem cokolwiek
				for (int i = stopienUporzadkowania; i < odlegloscPierwszej - 2; i++)
				{
					if (pochylnia->pochylnia[i] != porzadkowanaKulka && pochylnia->pochylnia[i + 1] == porzadkowanaKulka)
					{
						znalezionoCiag = true;
						pochylnia->przestawKulki(i);
						break;
					}
				}
			}
			else
			{
				// szukamy dobrej a potem cokolwiek byleby nie zla->dobra
				for (int i = stopienUporzadkowania; i < odlegloscPierwszej - 2; i++)
				{
					if ((pochylnia->pochylnia[i] == porzadkowanaKulka && pochylnia->pochylnia[i + 1] == porzadkowanaKulka && pochylnia->pochylnia[i + 2] == porzadkowanaKulka)
						|| (pochylnia->pochylnia[i] == porzadkowanaKulka && pochylnia->pochylnia[i + 1] == porzadkowanaKulka && pochylnia->pochylnia[i + 2] != porzadkowanaKulka)
						|| (pochylnia->pochylnia[i] == porzadkowanaKulka && pochylnia->pochylnia[i + 1] != porzadkowanaKulka && pochylnia->pochylnia[i + 2] != porzadkowanaKulka))
					{
						znalezionoCiag = true;
						pochylnia->przestawKulki(i);
						break;
					}
				}
			}
			if (znalezionoCiag == false)								// jesli nie znaleziono szukanego ciagu - przestawiamy pierwsza jeszcze nieuporzadkowana trojke
				pochylnia->przestawKulki(stopienUporzadkowania);
		}

		int doKtoregoSzukac = iloscKulek - 3;
		for (int i = stopienUporzadkowania; i < doKtoregoSzukac; i++)			// wyjmujemy trojki zlych spomiedzy dobrych
		{
			if (pochylnia->pochylnia[i] != porzadkowanaKulka && pochylnia->pochylnia[i + 1] != porzadkowanaKulka && pochylnia->pochylnia[i + 2] != porzadkowanaKulka)
			{
				pochylnia->przestawKulki(i);
				i--;
				doKtoregoSzukac -= 3;
			}
		}
		for (int i = stopienUporzadkowania; i < iloscKulek; i++)			// zwiekszamy stopienUporzadkowania
		{
			if (pochylnia->pochylnia[i] == porzadkowanaKulka)
				stopienUporzadkowania++;
			else
				break;
		}
		/* Teraz uruchamiamy Algorytm Dobrego Przestawienia w celu wyeliminowania niewielkich pozostalych niedoskonalosci porzadkowania */
		for (int i = stopienUporzadkowania; i < iloscKulek; ++i)
		{
			if (pochylnia->pochylnia[i] == porzadkowanaKulka)
			{
				czyBylMax = false;
				if (i != stopienUporzadkowania)
				{
					jakaOdleglosc = (iloscKulek - stopienUporzadkowania) % 3;
					ilePrzesuniec = (iloscKulek - stopienUporzadkowania - 1) / 3;
					if (jakaOdleglosc == 0)
					{
						pochylnia->przestawKulki(i);
						for (int j = 0; j < ilePrzesuniec; ++j)
						{
							pochylnia->przestawKulki(stopienUporzadkowania);
						}
						++stopienUporzadkowania;
					}
					else if (jakaOdleglosc == 1)
					{
						if (i - 2 >= 0 && i - 2 >= stopienUporzadkowania)
						{
							pochylnia->przestawKulki(i - 2);
							for (int j = 0; j < ilePrzesuniec; ++j)
							{
								pochylnia->przestawKulki(stopienUporzadkowania);
							}
							++stopienUporzadkowania;
						}
						else
							pochylnia->przestawKulki(i - 1);
					}
					else if (jakaOdleglosc == 2)
					{
						pochylnia->przestawKulki(i - 1);
						for (int j = 0; j < ilePrzesuniec; ++j)
						{
							pochylnia->przestawKulki(stopienUporzadkowania);
						}
						++stopienUporzadkowania;
					}
				}
				else
					++stopienUporzadkowania;
				i = stopienUporzadkowania - 1;
			}
			else if (i == iloscKulek - 3)		
			{
				if (czyBylMax == false)					
				{
					czyBylMax = true;
					i = stopienUporzadkowania - 1;
					pochylnia->przestawKulki(stopienUporzadkowania);
				}
				else								
					break;
			}
		}
		if (porzadkowanaKulka == 'c')
		{
			czyBylMax = false;
			porzadkowanaKulka = 'z';
		}
		else
			return;
	}
}

void algorytmNaiwny(Pochylnia* pochylnia)
{
	int iloscKulek = pochylnia->iloscKulek;
	int stopienUporzadkowania = 0;
	char porzadkowanaKulka = 'c';
	int ilePrzesuniec = 0;
	int odleglosc = 0;
	int staryStopienUporzadkowania = stopienUporzadkowania;


	while (stopienUporzadkowania < iloscKulek - 3)
	{
		staryStopienUporzadkowania = stopienUporzadkowania;
		odleglosc = iloscKulek - stopienUporzadkowania;
		if (odleglosc % 3 == 0)								// jesli liczba elementow do przeszukania jest podzielna przez 3 musimy w inny sposob przerzucac elementy
		{
			ilePrzesuniec = (odleglosc / 3 - 1) + (odleglosc - odleglosc / 3) * (1 + odleglosc / 3);
			for (int ilePrzesunieto = 0; ilePrzesuniec >= 0; ilePrzesuniec--)
			{
				if (pochylnia->pochylnia[stopienUporzadkowania] == porzadkowanaKulka)
				{
					stopienUporzadkowania++;
					break;
				}
				if (ilePrzesunieto == odleglosc / 3)	// co jakis czas przesuwamy kulki poczynajac od drugiej nieuporzadkowanej aby ostatecznie wszystkie znalazly sie na miejscu [stopienUporzadkowania]
				{
					ilePrzesunieto = 0;
					pochylnia->przestawKulki(stopienUporzadkowania + 1);
				}
				else
				{
					pochylnia->przestawKulki(stopienUporzadkowania);
					ilePrzesunieto++;
				}
			}
		}
		else
		{
			for (ilePrzesuniec = odleglosc - 1; ilePrzesuniec > 0; ilePrzesuniec--)		// jesli liczba elementow jest podzielna przez 3 wystarczy je przerzucac ciagle w ten sposob
			{
				if (pochylnia->pochylnia[stopienUporzadkowania] == porzadkowanaKulka)
				{
					stopienUporzadkowania++;
					break;
				}
				pochylnia->przestawKulki(stopienUporzadkowania);
			}
		}
		if (staryStopienUporzadkowania == stopienUporzadkowania)
		{
			if (porzadkowanaKulka == 'c')
			{
				porzadkowanaKulka = 'z';
			}
			else
				return;
		}
	}
}

void testAutomatyczny(void(*funkcja) (Pochylnia*), int algorytm, int doIlu, int ileRazy)
{
	float czasStartu = 0, czasDzialania = 0;
	char *kulki;
	int a = 50;
	float czasMax = 0, max = 0;
	if (algorytm == 0)
	{
		wyniki << "ALGORYTM NAIWNY" << endl;
	}
	else if (algorytm == 1)
	{
		wyniki << "ALGORYTM DOBREGO PRZESTAWIENIA" << endl;
	}
	else
	{
		wyniki << "ALGORYTM GRUPOWANIA" << endl;
	}
	wyniki << "n\tt" << endl;
	for (int ile = 200; ile <= doIlu; ile = ile + a, czasMax=0, max = 0)
	{
		for (int i = 0; i < ileRazy; i++)
		{
			kulki = new char[ile];
			generatorKulek(ile, kulki);
			Pochylnia pochylnia(ile);
			pochylnia.ustawKulki(kulki);
			czasStartu = clock();
			funkcja(&pochylnia);
			czasDzialania = clock() - czasStartu;
			czasMax = czasDzialania > czasMax ? czasDzialania : czasMax;
			max = pochylnia.ilePrzestawien > max ? pochylnia.ilePrzestawien : max;
			/*if (ile == 900 && i == 0)				// udowodnienie poprawnosci
				pochylnia.wypiszKulki();*/
			delete(kulki);
		}
		wyniki << ile << "\t" << czasMax / CLOCKS_PER_SEC << endl;
		cout << "Maksymalna ilosc przestawien dla " << ile << " kulek: " << max << endl;
		cout << "Maksymalny czas dla " << ile << " kulek: " << czasMax / CLOCKS_PER_SEC << "s" << endl;
		if (ile == 1000)
		{
			a = 1000;
		}
	}
	wyniki << endl;
}

void testUzytkownika()
{
	int opcja;
	char* kulki;
	ifstream plikWejsciowy;
	string filename;
	char c;
	int ile;
	cout << "Wybierz opcje:\n 1)Przetestowanie ciagu z pliku\n 2)Przetestowanie losowego ciagu" << endl;
	cin >> opcja;
	if (opcja == 1)
	{
		cout << "Podaj nazwe pliku wejsciowego" << endl;
		cin >> filename;
		cout << "Ile kulek zawiera plik?" << endl;
		cin >> ile;
		kulki = new char[ile];
		Pochylnia pochylnia(ile);
		plikWejsciowy.open(filename, ifstream::in);
		for (int i = 0; i < ile; i++)
		{
			plikWejsciowy >> c;
			while (c != 'c' && c != 'z' && c != 'n')
				plikWejsciowy >> c;
			kulki[i] = c;
		}
		pochylnia.ustawKulki(kulki);
		cout << "Wygenerowana pochylnia:" << endl;
		pochylnia.wypiszKulki();
		cout << "ALGORYTM NAIWNY" << endl;
		algorytmNaiwny(&pochylnia);
		pochylnia.ustawKulki(kulki);
		cout << "ALGORYTM DOBREGO PRZESTAWIENIA" << endl;
		algorytmDobregoPrzestawienia(&pochylnia);
		pochylnia.ustawKulki(kulki);
		cout << "ALGORYTM GRUPOWANIA" << endl;
		algorytmGrupowania(&pochylnia);
		cout << "KONIEC TESTOW" << endl;
		plikWejsciowy.close();
	}
	else if (opcja == 2)
	{
		cout << "Podaj wielkosc instancji problemu(ilosc kulek na pochylni)" << endl;
		cin >> ile;
		kulki = new char[ile];
		generatorKulek(ile, kulki);
		Pochylnia pochylnia(ile);
		pochylnia.ustawKulki(kulki);
		cout << "Wygenerowana pochylnia:" << endl;
		pochylnia.wypiszKulki();
		cout << "ALGORYTM NAIWNY" << endl;
		algorytmNaiwny(&pochylnia);
		pochylnia.ustawKulki(kulki);
		cout << "ALGORYTM DOBREGO PRZESTAWIENIA" << endl;
		algorytmDobregoPrzestawienia(&pochylnia);
		pochylnia.ustawKulki(kulki);
		cout << "ALGORYTM GRUPOWANIA" << endl;
		algorytmGrupowania(&pochylnia);
		cout << "KONIEC TESTOW" << endl;
	}
}