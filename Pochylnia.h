/*
Plik Pochylnia.h
Zadanie: Szeregowanie kulek na pochylni
Autor: Micha� Mudel
Prowadzacy: mgr in�. Pawe� Radziszewski
*/

class Pochylnia
{
public:
	Pochylnia();
	Pochylnia(int n);
	void ustawKulki(char*);
	int przestawKulki(int n);
	void wypiszKulki();
	~Pochylnia();

	int iloscKulek;
	char* pochylnia;
	int ilePrzestawien;
};

