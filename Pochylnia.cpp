/*
Plik Pochylnia.cpp
Zadanie: Szeregowanie kulek na pochylni
Autor: Micha� Mudel
Prowadzacy: mgr in�. Pawe� Radziszewski
*/

#define TES

#include "Pochylnia.h"
#include <iostream>

using namespace std;


Pochylnia::Pochylnia()
{
}


Pochylnia::~Pochylnia()
{
}

Pochylnia::Pochylnia(int n)
{
	iloscKulek = n;
	pochylnia = new char[n+3];
	ilePrzestawien = 0;
}

void Pochylnia::ustawKulki(char* kulki)
{
	for (int i = 0; i < iloscKulek; i++)
	{
		pochylnia[i] = kulki[i];
	}
	ilePrzestawien = 0;
}

int Pochylnia::przestawKulki(int n)
{
	if (n + 3 > iloscKulek)
		return 1;
#ifdef TEST
	cout << "Kolejne przestawienie" << endl;
	cout << "Przed przestawieniem: " << endl;
	this->wypiszKulki();
	for (int i = 0; i < n; i++)
		cout << "  ";
#endif
	for (int i = 0; i < 3; i++)
	{
		pochylnia[iloscKulek + i] = pochylnia[n + i];
#ifdef TEST
		cout << pochylnia[n + i] << " ";
#endif
	}
#ifdef TEST
	cout << endl;
#endif
	for (int i = n; i < iloscKulek; i++)
	{
		pochylnia[i] = pochylnia[i + 3];
	}
#ifdef TEST
	cout << "Po przestawieniu: " << endl;
	this->wypiszKulki();
	cout << endl;
#endif
	ilePrzestawien++;
	return 0;
}

void Pochylnia::wypiszKulki()
{
	for (int i = 0; i < iloscKulek; i++)
	{
		cout << pochylnia[i] << " ";
	}
	cout << endl;
}